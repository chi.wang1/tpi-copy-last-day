# 使用 chrome extension 加強工時系統

<https://gitlab.com/chi.wang1/tpi-copy-last-day>

## 前言

相信很多人都有用過 Chrome Extension，不管是 Youtube 擋廣告的 ADB 或是開發 Vue 的 dev tools 和看公司評價的求職天眼通等等...讓 chrome 這個瀏覽器相較其他瀏覽器加了不少分數，讓我們開始實作吧

## 功能

新增一顆按鈕，可以取最後一日資料，自動填入新增資料方便修改

## 需要技術

1. javascript
1. html
1. css
1. 使用 chrome F12 解析 js 和 html 的能力

## 建立檔案

建立資料夾 copy_last_day，在資料夾內建立檔案 manifest.json，填入以下 JSON 資料

<pre>
{
  "manifest_version": 2, //目前使用版本為2
  "name": "工時複製", //外掛名稱
  "description": "複製最後一日", //外掛描述
  "version": "1.0.0", //此外掛的版本
  "content_scripts": [
    {
      "matches": [
        "https://www.elite-erp.com.tw:8443/erp/workHour/work/addWorkHomeDynamic" //inject的頁面
      ],
      "js": [
        "contentScript.js" //inject程式
      ]
    }
  ]
}
</pre>

建立 contentScript.js 填入以下程式碼

<pre>
// 時間戳轉成y/m/d
const getYmd = timeInt => {
  const date = new Date(timeInt);
  const y = date.getFullYear();
  const m = date.getMonth() + 1;
  const d = date.getDate();
  return `${y}/${m}/${d}`;
};
// ajax queryWork 取資料的前置
const queryWork = callback => {
  const xhr = new XMLHttpRequest();
  xhr.open(
    "get",
    "https://www.elite-erp.com.tw:8443/erp/workHour/work/queryWork",
    true
  );
  xhr.onload = callback;
  xhr.send();
};
// ajax 取資料可以選擇日期
const doQueryWork = callback => {
  const f = new FormData();
  f.append("empCode", document.querySelector("#workFormList0\\.empCode").value);
  f.append(
    "copCode",
    document.querySelector("#workFormList0\\.refCopCode").value
  );
  const now = Date.now();
  f.append("startDate", getYmd(now - 30 * 60 * 60 * 24 * 1000));
  f.append("endDate", getYmd(now));
  const xhr = new XMLHttpRequest();
  xhr.open(
    "post",
    "https://www.elite-erp.com.tw:8443/erp/workHour/work/doQueryWork",
    true
  );
  xhr.onload = callback.bind(xhr);
  xhr.send(f);
};
// 解析出option的選項
const findOption = (doms, value) => {
  for (let i = 0; i < doms.length; i++) {
    if (doms[i].innerText.indexOf(value) != -1) {
      return doms[i].value;
    }
  }
};
// 解析資料頁
const parseHtml = html => {
  const list = [];
  const htmlDom = document.createElement("html");
  htmlDom.innerHTML = html;
  const tbodyDom = htmlDom.querySelectorAll("tbody tr");
  const pjCustCodeList = document.querySelectorAll(
    "#workFormList0\\.pjCustCode option"
  );
  const refPjCodeList = document.querySelectorAll(
    "#workFormList0\\.refPjCode option"
  );
  const refTaskIdList = document.querySelectorAll(
    "#workFormList0\\.refTaskId option"
  );
  for (let i = 0; i < tbodyDom.length; i++) {
    const item = tbodyDom[i];
    const rows = item.querySelectorAll("td");
    const name = rows[0].innerText.trim();
    let refTaskId = rows[4].innerText.trim().replace(/more$/, "");
    refTaskId = findOption(refTaskIdList, refTaskId);
    const tmp = rows[5].innerHTML.trim().split("<br>");
    const date = tmp[0];
    const tmp1 = tmp[1].split("~");
    const timeStart = tmp1[0];
    const timeEnd = tmp1[1];
    let pjCustCode = rows[1].innerText.trim();
    pjCustCode = findOption(pjCustCodeList, pjCustCode);
    let refPjCode = rows[2].innerText.trim();
    refPjCode = findOption(refPjCodeList, refPjCode);
    const workhours = rows[6].innerText.trim();
    const elementId = rows[4]
      .querySelector("[data-target]")
      .getAttribute("data-target");
    const workContent = htmlDom
      .querySelector(elementId + " .modal-body")
      .innerText.trim();
    if (rows[7].innerHTML.match(/modifyWorkHourDynamic\(([\d]+)\)/)) {
      var id = RegExp.$1;
      list.push({
        id,
        name,
        date,
        timeStart,
        timeEnd,
        workhours,
        refTaskId,
        workContent,
        refPjCode,
        pjCustCode
      });
    }
  }
  return list;
};
// 取得最後一日
const getLastDay = (list) => {
  const lastDay = [];
  if (list.length) {
    const objectDate = {};
    for (const i in list) {
      const item = list[i];
      if (list[0].date != item.date) continue;
      lastDay.push({
        workhours: item.workhours,
        refTaskId: item.refTaskId,
        workContent: item.workContent,
        refPjCode: item.refPjCode,
        pjCustCode: item.pjCustCode
      });
    }
    return lastDay;
  } else {
    alert("30日內都沒有上班喔");
  }
}
// 把資料填入
const insertContent = (lastDay) => {
  for (let i = 0; i < lastDay.length; i++) {
    const item = lastDay[i];
    document.querySelector("#workFormList" + i + "\\.pjCustCode").value =
      item.pjCustCode;
    document.querySelector("#workFormList" + i + "\\.refPjCode").value =
      item.refPjCode;
    document.querySelector("#workFormList" + i + "\\.refTaskId").value =
      item.refTaskId;
    document.querySelector("#workFormList" + i + "\\.workhours").value =
      item.workhours;
    document.querySelector("#workFormList" + i + "\\.workContent").value =
      item.workContent;
    document.querySelector(".btn-add").click();
  }
  document.querySelector("#row" + lastDay.length).remove();
}
const dom = document.createElement("button");
dom.className = "btn btn-blue";
dom.innerHTML = "複製最後一日(30日內)";
dom.onclick = e => {
  e.preventDefault();
  // 要先做這個ajax不然doQueryWork會給你500錯誤
  queryWork(() => {
    // 取得資料頁
    doQueryWork(function () {
      // 解析資料頁成array
      const list = parseHtml(this.response);
      // 取得最後一日
      const lastDay = getLastDay(list);
      // 把資料填入
      if(lastDay){
        insertContent(lastDay);
        dom.remove()
      }
    });
  });
};
document.querySelector("#form .text-center").append(dom);
</pre>

## 匯入外掛

![avatar](img/img1.png)

在網址輸入<chrome://extensions/>，點選載入未封裝項目選擇專案資料夾 copy_last_day

## 測試

進入工時系統<https://www.elite-erp.com.tw:8443/erp/workHour/work/addWorkHomeDynamic>
![avatar](img/img2.png)
最下排會出現"複製最後一日"，按鈕點下去後，會複製最近一日的工時內容

## 參考資料

1. <https://blog.longwin.com.tw/2014/01/chrome-extensions-plugin-develop-2014/>
1. <https://buzzorange.com/techorange/2017/09/21/qollie-closed/>
1. <https://developer.chrome.com/extensions>
